---
title: "Rice straws"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description: ""

# product Price
price: "20.00"
priceBefore: ""

# Product Short Description
shortDescription: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut"

#product ID
productID: "3"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/rice.png"
---

Our rice straws are 100% made from rice. They are 100 percent natural, biodegradable, compostable, affordable, and even edible; some rice straws can be cooked and consumed like a rice noodle.

They can last in cold drinks for up to 1 hour and don’t disintegrate or affect taste like paper straws.
