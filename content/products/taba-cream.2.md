---
title: "Grass straws"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description: ""

# product Price
price: "20.00"
priceBefore: "25.00"

# Product Short Description
shortDescription: "Made of 100% organic plants that are a plentiful renewable resource."

#product ID
productID: "2"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/grass.png"
---

Made of 100% organic plants that are a plentiful renewable resource. This grows super fast requiring very little energy. No chemical treatments were used ever on this straws.

Our grass straws are carefully chosen, cut, cleaned, sun dried, salt cured, oven baked and quality inspected in the paddy fields in Vietnam.
