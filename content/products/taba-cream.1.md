---
title: "Bambo straws"
date: 2019-10-17T11:22:16+06:00
draft: false

# meta description
description: ""

# product Price
price: "20.00"
priceBefore: "25.00"

# Product Short Description
shortDescription: "Hand made in Vietnam, the bamboo originates from from controlled organic farming and is processed without pesticides or other chemicals."

#product ID
productID: "1"

# type must be "products"
type: "products"

# product Images
# first image will be shown in the product page
images:
  - image: "images/banner.png"
---

Hand made in Vietnam, the bamboo originates from from controlled organic farming and is processed without pesticides or other chemicals. Our straws are also designed by local artisans. Each straw comes from a different bamboo shoot, which makes it unique in its colour and its diameter.
